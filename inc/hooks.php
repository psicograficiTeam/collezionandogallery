<?php
/**
 * Custom hooks.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! function_exists( 'understrap_site_info' ) ) {
  /**
   * Add site info hook to WP hook library.
   */
  function understrap_site_info() {
    do_action( 'understrap_site_info' );
  }
}


  /**
   * Add site Header
   */

  function understrap_add_site_header() {
    if (is_front_page()) {
      $headerTitle = get_theme_mod( 'header-title' );
      $headerSubtitle = get_theme_mod( 'header-subtitle' );
      $headerImageID = get_theme_mod('header-image');
      $headerImage = wp_get_attachment_url($headerImageID);

        echo '<header class="masthead py-5" style="background: url('.$headerImageID.') center center; background-size: cover;">';
        echo '<div class="container d-flex h-100 align-items-center pennellata">';
        echo '<div class="mx-auto text-center">';
        echo '<h1 class="mx-auto my-0 text-uppercase">'. $headerTitle .'</h1>';
        echo '<h2 class="mx-auto mt-2 mb-5">'. $headerSubtitle .'</h2>';
        //echo '<a href="#about" class="btn btn-primary js-scroll-trigger">Get Started</a>';
        echo '</div>';
        echo '</div>';
        echo '<div class="text-center"><img src="'.get_template_directory_uri().'/images/scroll-icon-header.png" class="my-0 mx-auto"></div>';
        echo '</header>';
      }
    } 
  add_action('understrap_site_header', 'understrap_add_site_header' );
   
/**
 *  ACF Google Maps
 */

function my_acf_init() {
	
	acf_update_setting('google_api_key', 'AIzaSyDJ65XpCIwGwzJkh2UjE7eITPBgN3h_yAs');
}

add_action('acf/init', 'my_acf_init');

/**
 *  Modifica read more text
 */

 function modify_read_more_link() {
    return '<a class="more-link" href="' . get_permalink() . '"> '.__('Read more', 'understrap').'</a>';
}
add_filter( 'the_content_more_link', 'modify_read_more_link' );

/**
 *  Cancella categoria dal titolo
 */

function prefix_category_title( $title ) {
    if ( is_category() ) {
        $title = single_cat_title( '', false );
    }
    return $title;
}
add_filter( 'get_the_archive_title', 'prefix_category_title' );

/**
* Display footer copyrights
**/

function copyrights_theme() {
	
	$copyright = get_theme_mod( 'copyright' );
	$psico_copyright = get_theme_mod( 'psico_copyright' );
	$anno = date('Y');

	if ($copyright) : 
	?>
		<div class="copyright mb-3"> <?php echo "@" . $anno . " " . $copyright; ?> </div>
		
	<?php endif; // End $copyright

	if($psico_copyright === true){ 
		printf( esc_html__( 'Realizzato con il %2$s da %1$s', 'Indica' ), '<a href="https://www.psicografici.com" rel="designer">Psicografici</a>', '<i class="fa fa-heart" style="color: #ff0000; padding:0;"></i> &amp; <i class="fa fa-wordpress" style="color:#369D95; padding:0;"></i>' );
	}

}
add_action('psicografici_site_footer', 'copyrights_theme' );


if ( ! function_exists( 'understrap_add_site_info' ) ) {
  add_action( 'understrap_site_info', 'understrap_add_site_info' );

  /**
   * Add site info content.
   */
  function understrap_add_site_info() {
    $the_theme = wp_get_theme();
    
    $site_info = sprintf(
      '<a href="%1$s">%2$s</a><span class="sep"> | </span>%3$s(%4$s)',
      esc_url( __( 'http://wordpress.org/', 'understrap' ) ),
      sprintf(
        /* translators:*/
        esc_html__( 'Proudly powered by %s', 'understrap' ), 'WordPress'
      ),
      sprintf( // WPCS: XSS ok.
        /* translators:*/
        esc_html__( 'Theme: %1$s by %2$s.', 'understrap' ), $the_theme->get( 'Name' ), '<a href="' . esc_url( __( 'http://understrap.com', 'understrap' ) ) . '">understrap.com</a>'
      ),
      sprintf( // WPCS: XSS ok.
        /* translators:*/
        esc_html__( 'Version: %1$s', 'understrap' ), $the_theme->get( 'Version' )
      )
    );

    echo apply_filters( 'understrap_site_info_content', $site_info ); // WPCS: XSS ok.
  }
}


add_action( 'pre_get_posts', 'my_change_sort_order'); 
    function my_change_sort_order($query){
        if(is_post_type_archive()):
         //If you wanted it for the archive of a custom post type use: is_post_type_archive( $post_type )
           //Set the order ASC or DESC
           $query->set( 'order', 'ASC' );
           //Set the orderby
           $query->set( 'orderby', 'title' );
        endif;    
    };