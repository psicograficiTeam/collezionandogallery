<?php
/**
 * Understrap Theme Customizer
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
if ( ! function_exists( 'understrap_customize_register' ) ) {
	/**
	 * Register basic customizer support.
	 *
	 * @param object $wp_customize Customizer reference.
	 */
	function understrap_customize_register( $wp_customize ) {
		$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
		$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
		$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

		// HEADER

		// Add Header Section
		$wp_customize->add_section( 'header' , array(
			'title' => __( 'Header', 'understrap' ),
			'priority' => 20,
			'description' => __( 'Modifica l\'header dell\'homepage del tema.', 'understrap' )
		) );

		// Add Background image
			$wp_customize->add_setting( 'header-image' , array( 'default' > '' ));
			$wp_customize->add_control( new WP_Customize_Image_Control(	$wp_customize, 'header-image', array(
			'label'      => __( 'Upload an image', 'understrap' ),
			'section'    => 'header',
			'settings'   => 'header-image'
			//'context'    => 'your_setting_context' 
			) )	);

		// Add Title Setting
			$wp_customize->add_setting( 'header-title' , array( 'default' > '' ));
			$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'header-title', array(
			'label' => __( 'Title', 'understrap' ),
			'section' => 'header',
			'settings' => 'header-title',
			) ) );

		// Add Subtitle Setting
			$wp_customize->add_setting( 'header-subtitle' , array( 'default' > '' ));
			$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'header-subtitle', array(
			'label' => __( 'Subtitle', 'understrap' ),
			'section' => 'header',
			'settings' => 'header-subtitle',
			) ) );


		// SOCIAL

		// Add Social Media Section
		$wp_customize->add_section( 'social-media' , array(
			'title' => __( 'Profili Social', 'understrap' ),
			//'priority' => 30,
			'description' => __( 'Modifica il tema per il centro selezionato.', 'understrap' )
		) );

		// Add Twitter Setting
			$wp_customize->add_setting( 'twitter' , array( 'default' > '' ));
			$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'twitter', array(
			'label' => __( 'Twitter', 'understrap' ),
			'section' => 'social-media',
			'settings' => 'twitter',
			) ) );

		// Add Facebook Setting
			$wp_customize->add_setting( 'facebook' , array( 'default' > '' ));
			$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'facebook', array(
				'label' => __( 'Facebook', 'understrap' ),
				'section' => 'social-media',
				'settings' => 'facebook',
			) ) );

			// Add Instagram Setting
			$wp_customize->add_setting( 'instagram' , array( 'default' > '' ));
			$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'instagram', array(
				'label' => __( 'Instagram', 'understrap' ),
				'section' => 'social-media',
				'settings' => 'instagram',
			) ) );

			// Add Pinterest Setting
			$wp_customize->add_setting( 'pinterest' , array( 'default' > '' ));
			$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'pinterest', array(
				'label' => __( 'Pinterest', 'understrap' ),
				'section' => 'social-media',
				'settings' => 'pinterest',
			) ) );

		// FOOTER

		// Add footer section
		$wp_customize->add_section( 'footer' , array(
		    'title' => __( 'Footer', 'understrap' ),
		    //'priority' => 30,
		    'description' => __( 'Modifica il copyright sul footer del tema.', 'understrap' )
		) );

		// Add Copyright Setting
		 $wp_customize->add_setting( 'copyright' , array( 'default' > '' ));
		 $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'copyright', array(
		    'label' => __( 'Copyright', 'understrap' ),
		    'section' => 'footer',
		    'settings' => 'copyright',
		 ) ) );



	    //  =============================
	    //  = Psicografici Copyrights   =
	    //  =============================

		$wp_customize->add_setting( 'psico_copyright', array(
		'default'        => true,
		'transport'  =>  'postMessage'
		 ) );

		$wp_customize->add_control('psico_copyright',
		array(
		    'section'   => 'footer',
		    'label'     => 'Copyright Psicografici',
		    'type'      => 'checkbox'
		     )
		 );
		 

	    //  =============================
	    //  = Select page   =
	    //  =============================

		$wp_customize->add_section( 'section' , array(
	    	'title'      => __( 'Homepage section', 'understrap' ),
			'priority'   => 30,
		) );

		$wp_customize->add_setting( 'section-1', array(
			'default'           => '',
			'sanitize_callback' => 'absint'
		) );
	
		$wp_customize->add_setting( 'section-2', array(
			'default'           => '',
			'sanitize_callback' => 'absint'
		) );

		$wp_customize->add_control( 'section-1', array(
			'label'    => __( 'Section 1', 'understrap' ),
			'section'  => 'section',
			'type'     => 'dropdown-pages'
		) );

		$wp_customize->add_control( 'section-2', array(
			'label'    => __( 'Section 2', 'understrap' ),
			'section'  => 'section',
			'type'     => 'dropdown-pages'
		) );

		//  =============================
	    //  = Select Category   =
	    //  =============================

		require_once get_stylesheet_directory() . '/inc/dropdown-category.php';
		//	$wp_customize->add_section( 'homepage', array(
		//		'title' => esc_html_x( 'Homepage Options', 'customizer section title', 'understrap' ),
		//	) );
		$wp_customize->add_setting( 'home_slider_category', array(
			'default'           => 0,
			'sanitize_callback' => 'absint',
		) );
		$wp_customize->add_control( new My_Dropdown_Category_Control( $wp_customize, 'home_slider_category', array(
			'section'       => 'section',
			'label'         => esc_html__( 'Slider posts category', 'understrap' ),
			'description'   => esc_html__( 'Select the category that the slider will show posts from. If no category is selected, the slider will be disabled.', 'understrap' ),
			// Uncomment to pass arguments to wp_dropdown_categories()
			//'dropdown_args' => array(
			//	'taxonomy' => 'post_tag',
			//),
		) ) );

		

	
	}
}
add_action( 'customize_register', 'understrap_customize_register' );

if ( ! function_exists( 'understrap_theme_customize_register' ) ) {
	/**
	 * Register individual settings through customizer's API.
	 *
	 * @param WP_Customize_Manager $wp_customize Customizer reference.
	 */
	function understrap_theme_customize_register( $wp_customize ) {

		// Theme layout settings.
		$wp_customize->add_section( 'understrap_theme_layout_options', array(
			'title'       => __( 'Theme Layout Settings', 'understrap' ),
			'capability'  => 'edit_theme_options',
			'description' => __( 'Container width and sidebar defaults', 'understrap' ),
			'priority'    => 160,
		) );

		/**
		 * Select sanitization function
		 *
		 * @param string               $input   Slug to sanitize.
		 * @param WP_Customize_Setting $setting Setting instance.
		 * @return string Sanitized slug if it is a valid choice; otherwise, the setting default.
		 */
        	function understrap_theme_slug_sanitize_select( $input, $setting ){

            		// Ensure input is a slug (lowercase alphanumeric characters, dashes and underscores are allowed only).
            		$input = sanitize_key( $input );

           		// Get the list of possible select options.
           		$choices = $setting->manager->get_control( $setting->id )->choices;

            		// If the input is a valid key, return it; otherwise, return the default.
            		return ( array_key_exists( $input, $choices ) ? $input : $setting->default );                

        	}

		$wp_customize->add_setting( 'understrap_container_type', array(
			'default'           => 'container',
			'type'              => 'theme_mod',
			'sanitize_callback' => 'understrap_theme_slug_sanitize_select',
			'capability'        => 'edit_theme_options',
		) );

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'understrap_container_type', array(
					'label'       => __( 'Container Width', 'understrap' ),
					'description' => __( 'Choose between Bootstrap\'s container and container-fluid', 'understrap' ),
					'section'     => 'understrap_theme_layout_options',
					'settings'    => 'understrap_container_type',
					'type'        => 'select',
					'choices'     => array(
						'container'       => __( 'Fixed width container', 'understrap' ),
						'container-fluid' => __( 'Full width container', 'understrap' ),
					),
					'priority'    => '10',
				)
			) );

		$wp_customize->add_setting( 'understrap_sidebar_position', array(
			'default'           => 'right',
			'type'              => 'theme_mod',
			'sanitize_callback' => 'sanitize_text_field',
			'capability'        => 'edit_theme_options',
		) );

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'understrap_sidebar_position', array(
					'label'       => __( 'Sidebar Positioning', 'understrap' ),
					'description' => __( 'Set sidebar\'s default position. Can either be: right, left, both or none. Note: this can be overridden on individual pages.',
					'understrap' ),
					'section'     => 'understrap_theme_layout_options',
					'settings'    => 'understrap_sidebar_position',
					'type'        => 'select',
					'sanitize_callback' => 'understrap_theme_slug_sanitize_select',
					'choices'     => array(
						'right' => __( 'Right sidebar', 'understrap' ),
						'left'  => __( 'Left sidebar', 'understrap' ),
						'both'  => __( 'Left & Right sidebars', 'understrap' ),
						'none'  => __( 'No sidebar', 'understrap' ),
					),
					'priority'    => '20',
				)
			) );
	}
} // endif function_exists( 'understrap_theme_customize_register' ).
add_action( 'customize_register', 'understrap_theme_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
if ( ! function_exists( 'understrap_customize_preview_js' ) ) {
	/**
	 * Setup JS integration for live previewing.
	 */
	function understrap_customize_preview_js() {
		wp_enqueue_script( 'understrap_customizer', get_template_directory_uri() . '/js/customizer.js',
			array( 'customize-preview' ), '20130508', true
		);
	}
}
add_action( 'customize_preview_init', 'understrap_customize_preview_js' );
