<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();

$container   = get_theme_mod( 'understrap_container_type' );

?>

<div class="wrapper" id="page-wrapper">

    <section id="storia" class="section-storia py-5">
        <div class="d-flex h-100 align-items-center pennellata-sx">

            <div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">
                <div class="row d-flex h-100 align-items-center text-center">
                    <?php 
                    $sectionId = get_theme_mod('section-1');
                    $my_query = new WP_Query('page_id='.$sectionId);
                    while ($my_query->have_posts()) : $my_query->the_post();
                    $do_not_duplicate = $post->ID;?> 
                        
                        <div class="col-md-6 col-12 order-12 order-md-1">
                            <?php the_post_thumbnail('full', ['class' => 'p-5']); ?>
                        </div>
                        <div class="col-md-6 col-12 order-1 order-md-12">
                                <?php the_title('<h3 class="text-uppercase title">', '</h3>'); ?>
                                <?php the_content(); ?>
                        </div>
                        
                        
                    
                    <?php endwhile; ?>   
                </div>
            </div>

        </div>
    </section>

    <section id="news" class="py-5">
            <?php
              $categoryId = get_theme_mod( 'home_slider_category' );
              $categoryLink = get_category_link( $categoryId );
            ?>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 text-center">
                    <h3 class="title text-uppercase"> <?php echo get_cat_name($categoryId);?></h3>
                </div>
                <?php 
                    // Example argument that defines three posts per page.
                    $stickies = get_option( 'sticky_posts' );

                    $args = array( 
                        'cat' => $categoryId,
                        'posts_per_page' => 3,
                        'post__in'            => $stickies,
                        'ignore_sticky_posts' => 1
                     );
                                        
                    $my_query = new WP_Query($args);
                    
                    while ($my_query->have_posts()) : $my_query->the_post();
                    $do_not_duplicate = $post->ID;?>
                    
                        <div class="col-md-4 col-12">
                            <div class="news-item d-flex h-100 align-items-center p-5" style="background: url(<?php echo the_post_thumbnail_url('full') ?>) center center no-repeat; background-size: cover;">
                                <div class="news-content d-flex h-100 align-items-center w-100 p-3 justify-content-center">
                                    <a href="<?php the_permalink(); ?>">
                                        <h3 class="text-uppercase d-block w-100 text-center">
                                            <?php the_title(); ?>
                                            <span class="h5 m-1 text-uppercase d-block"><?php the_field('data_evento'); ?></span>
                                            
                                        </h3>
                                    </a>
                                </div>    
                            
                            </div>
                        </div>
                        
                <?php endwhile; ?>

                        <div class="col-12 text-center">    
                            <a href="<?php echo esc_url($categoryLink) ?>" class="more-link"><?php echo __('Scopri di più', 'understrap'); ?></a>
                        </div>

            </div>
        </div>
    </section>

    <section id="ultimiarrivi" class="section-ultimiarrivi">
        <div class="d-flex h-100 align-items-center pennellata-dx">

            <div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">
                <div class="row d-flex h-100 align-items-center text-center">
                    <?php 
                    $sectionId = get_theme_mod('section-2');
                    $my_query = new WP_Query('page_id='.$sectionId);
                    while ($my_query->have_posts()) : $my_query->the_post();
                    $do_not_duplicate = $post->ID;?> 

                         <div class="col-md-6 col-12 order-12 order-md-1">
                            <h3 class="text-uppercase title">Ultimi arrivi</h3>
                            <?php the_content(); ?>
                        </div> 

                        <div class="col-md-6 col-12 order-1 order-md-12">
                            <?php the_post_thumbnail('full', ['class' => 'p-5']); ?>
                        </div>
                    
                    <?php endwhile; ?>   
                </div>
            </div>

        </div>
    </section>

    <section id="mappa" class="py-5 section-mappa">
        <div class="d-flex h-100 align-items-center text-center">
            <div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

                <div class="row">

                    <div class="col-12">
                        <h3 class="title text-uppercase">Ti aspettiamo nelle nostri sedi</h3>
                        <a href="#about" class="btn btn-primary js-scroll-trigger text-uppercase">Vai alla mappa</a>
                    </div>

                </div><!-- .row -->

            </div><!-- Container end -->
        </div>
    </section>



</div><!-- Wrapper end -->

<?php get_footer(); ?>
