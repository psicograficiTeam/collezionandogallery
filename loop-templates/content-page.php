<?php
/**
 * Partial template for content in page.php
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<article <?php post_class('row'); ?> id="post-<?php the_ID(); ?>">

	<header class="entry-header col-12">

		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		<hr class="my-5">
	</header><!-- .entry-header -->

	

	<div class="col-sm-8 col-12 py-sm-3 pb-3 entry-content">

		<?php the_content(); ?>

		<?php
		wp_link_pages( array(
			'before' => '<div class="page-links">' . __( 'Pages:', 'understrap' ),
			'after'  => '</div>',
		) );
		?>

	</div><!-- .entry-content -->
	<div class="col-sm-4 col-12">
		<?php echo get_the_post_thumbnail( $post->ID, 'large' ); ?>
	</div>
	<footer class="col-12 entry-footer">
	<hr class="my-5">
		<?php edit_post_link( __( 'Edit', 'understrap' ), '<span class="edit-link">', '</span>' ); ?>

	</footer><!-- .entry-footer -->

</article><!-- #post-## -->
