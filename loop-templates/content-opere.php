<?php
/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<article <?php post_class('grid-item col-md-4 col-sm-6 col-12 mb-3'); ?> id="post-<?php the_ID(); ?>">
    <div class="card">
                <?php 
                $img_attr = array(
                'src'	=> $src,
                'class'	=> "img-fluid",
                'alt'	=> trim(strip_tags( $attachment->post_excerpt )),
                'title'	=> trim(strip_tags( $attachment->post_title )),
                );
                echo get_the_post_thumbnail( $post->ID, 'large', $img_attr ); 
            ?>
            <div class="card-body">
                <?php the_title( sprintf( '<h5 class="entry-title text-uppercase"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ),
                            '</a></h5>' ); 
                            
                           
                    $post_object = get_field('artista');
                    if( $post_object ): 
                        // override $post
                        $post = $post_object;
                        setup_postdata( $post ); 
                        ?>
    	                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
 
                    <?php wp_reset_postdata(); ?>
                    <?php endif; ?>
                           
                           
                        
                            
            </div>
    </div>
</article>
