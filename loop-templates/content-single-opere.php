<?php
/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>
            
              <!--  -->
              <!-- <p class="card-text mb-auto">This is a wider card with supporting text below as a natural lead-in to additional content.</p>
              <a href="#">Continue reading</a> -->
            
<article <?php post_class('col-8 mx-auto mb-4'); ?> id="post-<?php the_ID(); ?>">
			<?php 
			$img_attr = array(
			'src'	=> $src,
			'class'	=> "card-img-left col-12 col-sm-8",
			'alt'	=> trim(strip_tags( $attachment->post_excerpt )),
			'title'	=> trim(strip_tags( $attachment->post_title )),
			);
			echo get_the_post_thumbnail( $post->ID, 'large', $img_attr ); 
		?>

	<div class="card-body col-12">
		<header class="entry-header">
			<strong class="d-inline-block mb-2 text-primary"><?php understrap_entry_footer(); ?></strong>
			<?php the_title( '<h3 class="mb-0 entry-title">', '</h3>' ); ?>
			

			<?php if ( 'post' == get_post_type() ) : ?>

				<div class="mb-1 text-muted entry-meta">
					<?php the_field('data_evento'); ?>
				</div><!-- .entry-meta -->

			<?php endif; ?>

		</header><!-- .entry-header -->

		<div class="my-3 entry-content">

			




			<ul class="list-group">
			<li class="list-group-item">


<?php 

						/*
						*  Query posts for a relationship value.
						*  This method uses the meta_query LIKE to match the string "123" to the database value a:1:{i:0;s:3:"123";} (serialized array)
						*/

						$doctors = get_posts(array(
							'post_type' => 'artisti',
							'meta_query' => array(
								array(
									'key' => 'opere', // name of custom field
									'value' => '"' . get_the_ID() . '"', // matches exactly "123", not just 123. This prevents a match for "1234"
									'compare' => 'LIKE'
								)
							)
						));

						?>
						<?php if( $doctors ): ?>
							<?php foreach( $doctors as $doctor ): ?>
							<strong>Artista:</strong>
									<a href="<?php echo get_permalink( $doctor->ID ); ?>">
										<?php echo get_the_title( $doctor->ID ); ?>
									</a>
							<?php endforeach; ?>
						<?php endif; ?>

				</li>
				<li class="list-group-item">
				
					<span class="d-block pb-2"><strong>Descrizione:</strong></span><?php the_content(); ?></li>
				<li class="list-group-item"><strong>Anno: </strong> <?php the_field('anno'); ?></li>
				<li class="list-group-item"><strong>Tecniche: </strong> <?php the_field('tecniche'); ?></li>
				<li class="list-group-item"><strong>Misure: </strong> <?php the_field('misure'); ?></li>
				<li class="list-group-item">
				<?php 

					$link = get_field('artsy');

					if( $link ): ?>
						
						<a class="w-80 btn btn-secondary btn-block	" href="<?php echo $link; ?>">Continua su Artsy</a>

					<?php endif; ?>
				</li>

			</ul>
 



			<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'understrap' ),
				'after'  => '</div>',
			) );
			?>

		</div><!-- .entry-content -->

	</div>
</article><!-- #post-## -->
