<?php
/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<article <?php post_class('col-12 my-5'); ?> id="post-<?php the_ID(); ?>">
	<div class="row d-flex align-items-center text-center" style="height: 30vh">
		
		<div class="col-12 col-sm-6 <?php echo (++$count % 2) ? "order-sm-1 order-12" : "order-sm-12 order-1"; ?>">
			<div class="artista-name">
				<?php the_title( sprintf( '<h3 class="entry-title text-uppercase"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ),
				'</a></h3>' ); ?>

			</div>
		</div>

		<div class="col-12 col-sm-6 h-100 <?php echo (++$count2 % 2) ? "order-sm-12 order-12" : "order-sm-1 order-1"; ?>">
			<div class="artista-image h-100" style="background: url(<?php echo get_the_post_thumbnail_url( $post->ID, 'large'); ?>)">
			</div>
		</div>

	</div>
</article><!-- #post-## -->
