<?php
/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>
            
              <!--  -->
              <!-- <p class="card-text mb-auto">This is a wider card with supporting text below as a natural lead-in to additional content.</p>
              <a href="#">Continue reading</a> -->
            
<article <?php post_class('col-8 mx-auto mb-4'); ?> id="post-<?php the_ID(); ?>">
			<?php 
			$img_attr = array(
			'src'	=> $src,
			'class'	=> "card-img-left col-12 col-sm-8",
			'alt'	=> trim(strip_tags( $attachment->post_excerpt )),
			'title'	=> trim(strip_tags( $attachment->post_title )),
			);
			echo get_the_post_thumbnail( $post->ID, 'large', $img_attr ); 
		?>

	<div class="card-body col-12">
		<header class="entry-header">
			<strong class="d-inline-block mb-2 text-primary"><?php understrap_entry_footer(); ?></strong>
			<?php the_title( '<h3 class="mb-0 entry-title">', '</h3>' ); ?>
			

			<?php if ( 'post' == get_post_type() ) : ?>

			<?php endif; ?>

		</header><!-- .entry-header -->

		<div class="my-3 entry-content">

                    <?php the_content();?>


                    <?php 

						$locations = get_field('opere');

						?>
						<?php if( $locations ): ?>
                            <h3>Le opere: </h3>
                            <div class="grid row">
                                
                            
							<?php foreach( $locations as $location ): ?>


                                <article <?php post_class('grid-item col-md-4 col-sm-6 col-12 mb-3'); ?> id="post-<?php the_ID(); ?>">
                                    <div class="card">
                                                <?php 
                                                $img_attr = array(
                                                'src'	=> $src,
                                                'class'	=> "img-fluid",
                                                'alt'	=> trim(strip_tags( $attachment->post_excerpt )),
                                                'title'	=> trim(strip_tags( $attachment->post_title )),
                                                );
                                                echo get_the_post_thumbnail( $location->ID, 'large', $img_attr ); 
                                            ?>
                                            <div class="card-body">
                                                <h5 class="entry-title text-uppercase">
                                                    <a href="<?php echo get_permalink( $location->ID ); ?>">
                                                            <?php echo get_the_title( $location->ID ); ?>
                                                        </a>                                   
                                                </h5>

                                            </div>
                                    </div>
                                </article>
                        
							<?php endforeach; ?>
                            </div>
						<?php endif; ?>

                         <?php 

                        $posts = get_posts(array(
                            'posts_per_page'	=> -1,
                            'post_type'			=> 'opere'
                        ));

                        if( $posts ): ?>
                            
                            <div class="row grid">
                                    
                                
                            <?php foreach( $posts as $post ): 
                                
                                setup_postdata( $post );
                                
                                ?>

                            <?php endforeach; ?>
                            
                            
                            </div>                            
                            <?php wp_reset_postdata(); ?>

                        <?php endif; ?>
			

			<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'understrap' ),
				'after'  => '</div>',
			) );
			?>

		</div><!-- .entry-content -->

	</div>
</article><!-- #post-## -->
