<?php
/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

            
<article <?php post_class('card flex-md-row mb-4 box-shadow h-md-250'); ?> id="post-<?php the_ID(); ?>">
			<?php 
				$img_attr = array(
				'src'	=> $src,
				'class'	=> "card-img-right flex-auto d-none d-md-block",
				'alt'	=> trim(strip_tags( $attachment->post_excerpt )),
				'title'	=> trim(strip_tags( $attachment->post_title )),
				);
				echo get_the_post_thumbnail( $post->ID, 'large', $img_attr ); 
			?>

	<div class="card-body d-flex flex-column align-items-start">
		<header class="entry-header">
			<strong class="d-inline-block mb-2 text-primary"><?php understrap_entry_footer(); ?></strong>
			<?php the_title( sprintf( '<h3 class="mb-0 entry-title"><a class="text-dark" href="%s" rel="bookmark">', esc_url( get_permalink() ) ),
			'</a></h3>' ); ?>

			<?php if ( 'post' == get_post_type() ) : ?>

				<div class="mb-1 text-muted entry-meta">
					<?php the_field('data_evento'); ?>
				</div><!-- .entry-meta -->

			<?php endif; ?>

		</header><!-- .entry-header -->

		<div class="entry-content">

			<?php
			the_excerpt();
			?>

			<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'understrap' ),
				'after'  => '</div>',
			) );
			?>

		</div><!-- .entry-content -->

	</div>
</article><!-- #post-## -->
