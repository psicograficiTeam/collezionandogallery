<?php
/**
 * Partial template for content in page.php
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<article <?php post_class('row'); ?> id="post-<?php the_ID(); ?>">

	<header class="entry-header col-12">

		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		<hr class="my-5">
	</header><!-- .entry-header -->

	
    <?php 
        $facebook = get_theme_mod('facebook');
        $twitter = get_theme_mod('twitter');
        $instagram = get_theme_mod('instagram');
        $pinterest = get_theme_mod('pinterest');
    ?>
    
    <div class="col-sm-6 col-12 py-sm-3 pb-3 entry-content">

        <?php the_content(); ?>
        
        <h3 class="text-uppercase"><?php _e('Seguici', 'understrap'); ?></h3>
        <a href="<?php echo $facebook; ?>">
            <span class="fa-stack fa-lg">
                <i class="fa fa-circle fa-stack-2x"></i>
                <i class="fa fa-facebook fa-stack-1x"></i>
            </span> 
    
        </a> 
        <a href="<?php echo $twitter; ?>">
            <span class="fa-stack fa-lg">
                <i class="fa fa-circle fa-stack-2x"></i>
                <i class="fa fa-twitter fa-stack-1x"></i>
            </span> 
        </a>

        <a href="<?php echo $instagram; ?>">
            <span class="fa-stack fa-lg">
                <i class="fa fa-circle fa-stack-2x"></i>
                <i class="fa fa-instagram fa-stack-1x"></i>
            </span> 
        </a>

        <a href="<?php echo $pinterest; ?>">
            <span class="fa-stack fa-lg">
                <i class="fa fa-circle fa-stack-2x"></i>
                <i class="fa fa-pinterest fa-stack-1x"></i>
            </span> 
        </a>
		<?php
		wp_link_pages( array(
			'before' => '<div class="page-links">' . __( 'Pages:', 'understrap' ),
			'after'  => '</div>',
		) );
		?>

	</div><!-- .entry-content -->
	<div class="col-sm-6 col-12">
        <?php 
            $formID = get_field('form')->ID;
            $formTitle = get_field('form')->Title;
            echo do_shortcode('[contact-form-7 id="'.$formID.'" title="'.$formTitle.'"]');
        ?>
	</div>
	<footer class="col-12 entry-footer">
	<hr class="my-5">
		<?php edit_post_link( __( 'Edit', 'understrap' ), '<span class="edit-link">', '</span>' ); ?>

	</footer><!-- .entry-footer -->

</article><!-- #post-## -->

