jQuery(document).ready(function () {
    jQuery(window).bind('scroll', function () {
        var navHeight = jQuery('.navbar-light').height();
        //console.log(navHeight);

        if (jQuery('body.home').length) {
            jQuery('nav').addClass('fixed-top');
            if (jQuery(window).scrollTop() > navHeight) {
                jQuery('nav').removeClass('bg-transparent');
                jQuery('nav').addClass('bg-fixed');
            }
            else {
                jQuery('nav').removeClass('bg-fixed');
                jQuery('nav').addClass('bg-transparent');
            }
        }
        else {
            if (jQuery(window).scrollTop() > navHeight) {
                jQuery('.navbar-light').removeClass('bg-transparent');
                jQuery('.navbar-light').addClass('bg-fixed');
                jQuery('.navbar-light').addClass('fixed-top');
            }
            else {
                jQuery('.navbar-light').removeClass('bg-fixed');
                jQuery('.navbar-light').removeClass('fixed-top');
                jQuery('.navbar-light').addClass('bg-transparent');
            }
        }

    


    });




});

// var $grid = jQuery('.grid').masonry({
//     // options...
// });
// $grid.imagesLoaded().progress(function () {
//     $grid.masonry('layout');
// });

//-------------------------------------//
// init Masonry


// with Masonry & jQuery

// init Masonry
// var $grid = jQuery('.grid').masonry({
//     // Masonry options...
//     itemSelector: '.grid-item',
// });

// // get Masonry instance
// var msnry = $grid.data('masonry');

// // initial items reveal
// $grid.imagesLoaded(function () {
//     //$grid.removeClass('are-images-unloaded');
//     $grid.masonry('option', { itemSelector: '.grid-item' });
//     var $items = $grid.find('.grid-item');
//     $grid.masonry('appended', $items);
// });

// // init Infinite Scroll
// $grid.infiniteScroll({
//     path: ".next",
//     append: '.grid-item',
//     outlayer: msnry,
//     hideNav: '.pagination',
// });